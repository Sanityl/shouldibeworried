var express = require('express');
var router = express.Router();
var twitter = require("../middleware/twitter.js");
var spSearch = require("../middleware/specialSearch.js");


/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Social Media Search' });
});

router.get('/specialSearch', function(req, res, next) {
  res.render('specialSearch', { title: 'Social Media Search' });
});


router.get('/search', function(req, res, next) {
  console.log("search");
  //console.log(req.query);
  console.log(req.query.term);
  twitter.searchTwit(req.query.term, returnData);

  function returnData(data){
    res.json(data)
    res.end();
  }

});

router.get('/spSearch', function(req, res, next) {
  console.log("special search");
  //console.log(req.query);
  console.log("term: "+req.query.term);
  spSearch.input(req.query.term, returnData);

  function returnData(data){
   console.log(data);
    res.json(data)
    res.end();
  }

});

module.exports = router;
