$(document).ready(function(){
    $("#searchBtn").on("click",function(){
        search($("#searchBox").val());
    })
    $("#spSearchBtn").on("click",function(){
        specialSearch($("#searchBox").val());
    })

    $("#searchBox").keypress(function(event) {
        if (event.which == 13) {
            event.preventDefault();
            $("#searchBox").submit();
            $( "#results" ).focus();
        }
    });
})
function search(searchItem){
    console.log("Search");
   var term = {term:searchItem};
    var output = $("#results");
    output.empty();
    $(".sk-cube-grid").toggleClass("hidden");
    $.get('/search',term, function(response) {
        var results = (response);
        console.log(results);
        displayOutput(results);
    },'json');

}
function newSearch(e){
    debugger;
    console.log(e.text);
}
function displayOutput(data){
    var htmlOutput="";
    var output = $("#results");
    $(".sk-cube-grid").toggleClass("hidden");
   // data = JSON.stringify(data);

    data.statuses.forEach(function(elem) {
        //console.log(elem.text);
        htmlOutput += '<div class="pure-u-1-3"><div class="gridBox">' + twitterize(elem.text) + '</div></div>';
    });
    output.append(htmlOutput);

}

function twitterize(text){
    var output;
    if (typeof text == 'undefined'){
        console.log("undefined text");
        return output;
    }

    output = text.replace(/(#)\w+/g,function(v){return '<a class="twitLink" onclick="newSearch(this)">'+v+'</a>'});
    //output = text.replace(/(https:)\w+/g,function(v){return '<a class="twitLink" onclick="newSearch(this)">'+v+'</a>'});
    //output = text;

    return output;
}
function specialSearch(searchItem){
    console.log("specialSearch");
    var term = {term:searchItem};
    var output = $("#results");
    output.empty();
    $(".sk-cube-grid").toggleClass("hidden");
    $.get('/spSearch',term, function(response) {
        var results = (response);
        console.log(results);
        displayOutput(results);
    },'json');

}