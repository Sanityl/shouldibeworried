/**
 * Created by SL on 21/03/2017.
 */
var exports = module.exports = {};
var MongoClient = require('mongodb').MongoClient
    , assert = require('assert');

// Connection URL
var url = 'mongodb://localhost:27017/wordDB';
// Use connect method to connect to the Server
var db;
MongoClient.connect(url, function (err, database) {
//    assert.equal(null, err);
    console.log("Connected correctly to server");
    db = database;
});


exports.connect =function(abc,callback) {
    MongoClient.connect(url, function (err, db) {
        assert.equal(null, err);
        console.log("Connected correctly to server");
        insertDocuments(db, function(err){
            if(err)console.log(err);
        });
        db.close();
    });
    callback("done");
}


exports.insert = function(term, callback){
    // Get the documents collection
    var collection = db.collection('words');
    // Insert some documents
    collection.insertMany([
        {a : 1}, {a : 2}, {a : 3}
    ], function(err, result) {
        assert.equal(err, null);
        assert.equal(3, result.result.n);
        assert.equal(3, result.ops.length);
        console.log("Inserted 3 documents into the document collection");
        callback(result);
    });
}

exports.findAll = function(term, callback){
    console.log("findAll");
    var collection = db.collection('words');
    collection.find({a:1}).toArray(function(err, docs) {
        assert.equal(err, null);
        console.log("Found the following records");
        console.dir(docs);
        callback(docs);
    });
}

exports.find = function(term, callback){
    console.log("find");
    var collection = db.collection('words');
    collection.find({a:1}).toArray(function(err, docs) {
        assert.equal(err, null);
        console.log("Found the following records");
        console.dir(docs);
        callback(docs);
    });
}
